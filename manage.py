import os
import unittest

from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from project import create_app
from project.blueprints import api_blueprint
from project.extensions import db
from project.flask_app.models import user, director, genre, movie

app = create_app(os.getenv('ENV_TYPE') or 'dev')
app.register_blueprint(api_blueprint)

app.app_context().push()

manager = Manager(app)

migrate = Migrate(app, db)

manager.add_command('db', MigrateCommand)

@manager.command
def run():
    app.run(host=os.getenv('HOST', '127.0.0.1'))

@manager.command
def test():
    """Runs the unit tests."""
    tests = unittest.TestLoader().discover('project/test', pattern='test*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    return 1

if __name__ == '__main__':
    manager.run()   # run the application