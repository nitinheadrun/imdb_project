from flask import Blueprint
from flask_restplus import Api
from project.flask_app.controllers.api.v1.users_api import api as user_namespace
from project.flask_app.controllers.api.v1.auth_api import api as auth_namespace
from project.flask_app.controllers.api.v1.movies_api import api as movie_namespace
from project.flask_app.controllers.api.v1.directors_api import api as director_namespace
from project.flask_app.controllers.api.v1.genres_api import api as genre_namespace

api_blueprint = Blueprint('api', __name__)
api = Api(api_blueprint, title="Flask IMDB api")

api.add_namespace(user_namespace, path="/user")
api.add_namespace(auth_namespace, path="/auth")
api.add_namespace(movie_namespace, path="/movies")
api.add_namespace(director_namespace, path="/directors")
api.add_namespace(genre_namespace, path="/genres")