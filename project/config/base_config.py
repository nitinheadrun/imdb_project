import os
basedir = os.path.join(os.path.abspath(os.path.dirname(__file__)), '..')

class Config:
    HOST = os.getenv('HOST', '127.0.0.1')
    SECRET_KEY = os.getenv('SECRET_KEY', 'some_random_secret_key')
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'app.db')
    DEBUG = False