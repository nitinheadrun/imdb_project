from flask import request
from functools import wraps
from project.flask_app.models.user import User 

def login_required(f):
    @wraps(f)
    def inner(*args, **kwargs):
        headers = request.headers
        token = request.headers.get('X-API_KEY')
        if not token:
            return {"status": "error", "message": "No token provided" }, 401
        user_payload = User.decode_auth_token(token)

        if isinstance(user_payload, str):
            return {"status": "errror", "message": user_payload}, 401
        return f(*args, **kwargs)
        
    return inner

def admin_login_required(f):
    @wraps(f)
    def inner(*args, **kwargs):
        headers = request.headers
        token = request.headers.get('X-API_KEY')
        if not token:
            return {"status": "error", "message": "No token provided" }, 401
        user_payload = User.decode_auth_token(token)
        
        if isinstance(user_payload, str):
            return {"status": "error", "message": user_payload}, 401
        
        if user_payload.get('is_admin', False) is False:
            return {"status": "error", "message": "You don't have admin privileges"}, 403
        return f(*args, **kwargs)
        
    return inner