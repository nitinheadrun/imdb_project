def model_serializer(modelInstance):
    columns = modelInstance.__table__.columns.keys()
    return {column: getattr(modelInstance, column) for column in columns}