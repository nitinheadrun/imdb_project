from project.extensions import db
from .genre import Genre

movie_genre = db.Table('movie_genre',
                       db.Column('movie_id', db.Integer,
                                 db.ForeignKey('movie.id')),
                       db.Column('genre_id', db.Integer,
                                 db.ForeignKey('genre.id'))
                       )


class Movie(db.Model):
    __tablename__ = 'movie'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(100), nullable=False)
    imdb_score = db.Column(db.Float, nullable=False, default=0.0)
    genres = db.relationship(Genre,
                             secondary=movie_genre,
                             backref=db.backref('movies', lazy='dynamic'),
                             lazy='dynamic')
    director_id = db.Column(db.Integer, db.ForeignKey('director.id'))

    @classmethod
    def get_all_movies(cls):
        return cls.query.all()

    @classmethod
    def create_movie(cls, name, imdb_score=0.0, genres=[], director_id=None):
        movie = cls.query.filter_by(name=name).first()
        if not movie:
            new_movie = Movie(
                name=name,
                imdb_score=imdb_score
            )
            db.session.add(new_movie)
            if director_id:
                new_movie.director_id = director_id
            db.session.commit()

            return {
                'status': 'success',
                'message': 'new movie added successfully',
                'movie': new_movie
            }, 201
        else:
            return {
                'status': 'error',
                'message': 'Movie already created',
                'movie': movie
            }, 409

    def get_genres(self):
        # return Genre.query.filter(Genre.movies.any(id=self.id)).all()
        return self.genres.all()
    
    @classmethod
    def get_movie(cls, id):
        return cls.query.get(id)

    def get_director(self):
        return self.director    # backref field in Director.movies

    def add_genre(self, genre):
        if not isinstance(genre, Genre):
            return {
                'status': 'error',
                'message': 'Either invalid genre. genre must be an instance of Genre'
            }, 409
        elif self.is_genre_added(genre):
            return {
                'status': 'error',
                'message': 'Either invalid genre. genre must be an instance of Genre'
            }, 409
        else:
            self.genres.append(genre)
            return {
                'status': 'success',
                'message': 'Genre added successfully'
            }, 203

    def add_or_create_genre(self, genre):
        if not isinstance(genre, Genre):
            response, status_code = Genre.get_or_create_genre(genre)
            if status_code in [200, 201]:
                self.genres.append(response['genre'])    
            return response, status_code
        elif self.is_genre_added(genre):
            return {
                'status': 'error',
                'message': 'Either invalid genre. genre must be an instance of Genre'
            }, 409
        else:
            self.genres.append(genre)
            return {
                'status': 'success',
                'message': 'Genre added successfully'
            }, 203
    
    def add_genre_list(self, genre_list):
        if isinstance(genre_list, list):
            self.genres.extend(genre_list)
            return {
                'status': 'success',
                'message': 'Genre added successfully'
            }, 203
        else:
            return {
                'status': 'error',
                'message': 'wrong parameter attribute, list is expected'
            }, 400

    def remove_genre(self, genre):
        if isinstance(genre, Genre) and self.is_genre_added(genre):
            self.genres.remove(genre)
    
    def is_genre_added(self, genre):
        return self.genres.filter(movie_genre.c.genre_id == genre.id).count() > 0

    def save_data(self, data=None):
        data = data if data else self
        db.session.add(data)
        db.session.commit()
    
    def remove_director(self):
        self.director = None 
        self.save_data(self)
        return {
            'status': 'success',
            'message': 'director deleted successfully'
        }, 200

    def delete_movie(self):
        db.session.delete(self)
        db.session.commit()
        return {
            'status': 'success',
            'message': 'movie deleted successfully'
        }, 200

    def __repr__(self):
        return f'\"{self.name}\" directed by {self.get_director()}'
