from flask_restplus import Resource
from project.flask_app.utils.auth_helper import AuthHelper
from project.flask_app.modelSchemas.auth_schema import AuthSchema

api = AuthSchema.api
auth_user = AuthSchema.auth_user

@api.route('/login')
class UserLogin(Resource):
    @api.doc('user login')
    @api.expect(auth_user, validate=True)
    def post(self):
        data = api.payload
        return AuthHelper.login(data)

