from flask_restplus import Resource
from project.flask_app.utils.decorators import login_required, admin_login_required
from project.flask_app.models.user import User
from project.flask_app.modelSchemas.user_schema import UserSchema

api = UserSchema.api
user_schema = UserSchema.user 

@api.route('/')
class Users(Resource):

    @api.doc('list of all users')
    @api.marshal_list_with(user_schema, envelope='data')
    def get(self):
        return User.get_all_users()

    @api.expect(user_schema, validate=True)
    @api.doc('create a new user')
    def post(self):
        data = api.payload
        return User.create_user(data)

@api.route('/<id>')
class UserDetail(Resource):
    @api.doc('get a user')
    @api.marshal_with(user_schema)
    def get(self, id):
        user = User.get_user(id)
        if not user:
            api.abort(404)
        return user, 200
    