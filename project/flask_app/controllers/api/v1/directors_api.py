from flask_restplus import Resource
from project.flask_app.utils.decorators import login_required, admin_login_required
from project.flask_app.models.director import Director
from project.flask_app.modelSchemas.director_schema import DirectorSchema

api = DirectorSchema.api
director_schema = DirectorSchema.directors

@api.route('/')
class Directors(Resource):
    @api.doc('list of directors')
    @api.marshal_list_with(director_schema, envelope="data")
    def get(self):
        return Director.get_all_directors(), 201

    @admin_login_required
    @api.doc('create new director', security="apikey")
    @api.expect(director_schema, validate=True)
    def post(self):
        data = api.payload
        return DirectorSchema.format_and_save(data)