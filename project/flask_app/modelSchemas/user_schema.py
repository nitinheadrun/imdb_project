from flask_restplus import Namespace, fields


class UserSchema:
    authorizations = {
        'apikey': {
            'type': 'apiKey',
            'in': 'header',
            'name': 'X-API-KEY'
        },
    }
    api = Namespace('user', description="user related operations",
                    authorizations=authorizations)

    user = api.model('user', {
        'email': fields.String(required=True, description="user email address"),
        'username': fields.String(required=True, description="user username"),
        'password': fields.String(required=True, description="user password"),
        'is_admin': fields.Boolean(required=False, default=False)
    })
