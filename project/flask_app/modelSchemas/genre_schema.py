from flask_restplus import Namespace, fields
from .base_schema import BaseModelSchema
from ..models.genre import Genre
from ..utils.serializers import model_serializer

class GenreSchema(BaseModelSchema):
    api = BaseModelSchema.create_namespace('genre', "genre related operations")

    genres = api.model('genre', {
        'id': fields.Integer("genre id", required=False),
        'name': fields.String("genre name", required=True)
    })

    @classmethod
    def format_and_save(cls, data):
        name = data['name']
        response, status_code = Genre.create_genre(name)
        genre = response.get('genre')

        return {
            'status': response['status'],
            'message': response['message'],
            'director': model_serializer(genre) if genre else None
        }, status_code


    @classmethod
    def format_edit_save(cls, genre, data):
        name = data['name']
        genre.name = name
        genre.save_data()
        return {
            'status': 'success',
            'message': 'successfully updated genre name',
            'genre': model_serializer(genre)
        }