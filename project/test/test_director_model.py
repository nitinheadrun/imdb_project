import unittest
from project.test.base import BaseTestCase
from project.flask_app.models.director import Director
from project.flask_app.models.movie import Movie

class TestDirectorModel(BaseTestCase):

    def test_create_director(self):
        response, status_code = Director.create_director("test")
        self.assertEqual(status_code, 201)
        self.assertEqual(response.get('status', 'error'), 'success')

    def test_add_movie(self):
        movie_reponse, movie_status_code = Movie.create_movie('horror')
        movie = movie_reponse['movie']
        self.assertEqual(movie_status_code, 201)


        director_response, director_status_code = Director.create_director('test director')
        self.assertEqual(director_status_code, 201)

        director = director_response['director']

        add_movie_resp, add_movie_code = director.add_movie(movie)
        self.assertEqual(add_movie_code, 203)

if __name__ == "__main__":
    unittest.main()