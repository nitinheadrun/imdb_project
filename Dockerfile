FROM python:3.6

# os setup
RUN apt-get update && apt-get -y install \
  python-lxml \
  build-essential \
  libssl-dev \
  libffi-dev \
  python-dev \
  libxml2-dev \
  libxslt1-dev \
  && rm -rf /var/lib/apt/lists/*
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# install requirements
COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt

RUN apt-get update && apt-get install -y libssl-dev
RUN apt-get install  -y python3-dev default-libmysqlclient-dev

RUN apt-get install -y default-mysql-client

# move codebase over
COPY .  /usr/src/app

# set up environment variables

# run the spider
CMD ["python3", "manage.py", "runserver"]